import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-sidebar',
  template: `
    <ul class="nav flex-column nav-pills" role="tablist">
      <li class="nav-item" role="presentation">
        <a class="nav-link"
           routerLinkActive="active"
           [routerLinkActiveOptions]="{exact: true}"
           routerLink="/">Basic Form</a>
      </li>
      <li class="nav-item" role="presentation">
        <a class="nav-link"
           routerLinkActive="active"
           routerLink="basic-validation">Basic Form Validation</a>
      </li>
      <li class="nav-item" role="presentation">
        <a class="nav-link"
           routerLinkActive="active"
           routerLink="async-validation">Async Form Validation</a>
      </li>
      <li class="nav-item" role="presentation">
        <a class="nav-link"
           routerLinkActive="active"
           routerLink="json-powered">JSON Powered Form</a>
      </li>
      <li class="nav-item" role="presentation">
        <a class="nav-link"
           routerLinkActive="active"
           routerLink="json-file-powered">Form from File</a>
      </li>
      <li class="nav-item" role="presentation">
        <a class="nav-link"
           routerLinkActive="active"
           routerLink="configurable-form">Configurable Form</a>
      </li>
      <li class="nav-item" role="presentation">
        <a class="nav-link"
           routerLinkActive="active"
           routerLink="dynamic-list">Dynamic List</a>
      </li>
    </ul>
  `,
  styles: ['.nav-link {font-size: small;}']
})
export class SidebarComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
