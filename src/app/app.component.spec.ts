import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {BasicFormComponent} from './formly-ext-module/basic-form/basic-form.component';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import {FormlyModule} from '@ngx-formly/core';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        BasicFormComponent
      ],
      imports: [
        ReactiveFormsModule,
        FormlyBootstrapModule,
        FormlyModule,
        RouterTestingModule
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
