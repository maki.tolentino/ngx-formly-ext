import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormlyExtModule} from './formly-ext-module/formly-ext.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SidebarComponent} from './sidebar/sidebar.component';

const routes: Routes = [{
  path: '',
  loadChildren: './formly-ext-module/formly-ext.module#FormlyExtModule'
}];

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    RouterModule.forRoot(routes),
    NgbModule,
    FormlyExtModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
