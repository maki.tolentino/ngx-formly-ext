import {NgModule} from '@angular/core';

import {RouterModule, Routes} from '@angular/router';
import {BasicFormComponent} from './basic-form/basic-form.component';
import {BasicValidationFormComponent} from './basic-validation-form/basic-validation-form.component';
import {AsyncValidationFormComponent} from './async-validation-form/async-validation-form.component';
import {JsonPoweredFormComponent} from './json-powered-form/json-powered-form.component';
import {FilePoweredFormComponent} from './file-powered-form/file-powered-form.component';
import {ConfigurableFormComponent} from './configurable-form/configurable-form.component';
import {DynamicListComponent} from './dynamic-list/dynamic-list.component';

const routes: Routes = [
  {path: '', component: BasicFormComponent},
  {path: 'basic-validation', component: BasicValidationFormComponent},
  {path: 'async-validation', component: AsyncValidationFormComponent},
  {path: 'json-powered', component: JsonPoweredFormComponent},
  {path: 'json-file-powered', component: FilePoweredFormComponent},
  {path: 'configurable-form', component: ConfigurableFormComponent},
  {path: 'dynamic-list', component: DynamicListComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormlyExtRoutingModule {
}
