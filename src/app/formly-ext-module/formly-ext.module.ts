import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import {BasicFormComponent} from './basic-form/basic-form.component';
import {BasicValidationFormComponent} from './basic-validation-form/basic-validation-form.component';
import {AsyncValidationFormComponent} from './async-validation-form/async-validation-form.component';
import {JsonPoweredFormComponent} from './json-powered-form/json-powered-form.component';
import {FilePoweredFormComponent} from './file-powered-form/file-powered-form.component';
import {JsonConfigService} from './common/json-config.service';
import {FormlyExtRoutingModule} from './formly-ext-routing.module';
import {ConfigurableFormComponent} from './configurable-form/configurable-form.component';
import {DynamicListComponent} from './dynamic-list/dynamic-list.component';
import {DynamicDetailsViewComponent} from './dynamic-details-view/dynamic-details-view.component';
import {DynamicListService} from "./common/dynamic-list.service";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

export function minValidationMessage(err, field) {
  return `This value should be more than ${field.templateOptions.min}`;
}

export function maxValidationMessage(err, field) {
  return `This value should be less than ${field.templateOptions.max}`;
}

@NgModule({
  imports: [
    CommonModule,
    FormlyExtRoutingModule,
    ReactiveFormsModule,
    FormlyBootstrapModule,
    NgbModule,
    FormlyModule.forRoot({
      validationMessages: [
        {name: 'required', message: 'This field is required'},
        {name: 'minlength', message: minValidationMessage},
        {name: 'maxlength', message: maxValidationMessage}
      ],
    })
  ],
  declarations: [
    BasicFormComponent,
    BasicValidationFormComponent,
    AsyncValidationFormComponent,
    JsonPoweredFormComponent,
    FilePoweredFormComponent,
    ConfigurableFormComponent,
    DynamicListComponent,
    DynamicDetailsViewComponent
  ],
  providers: [
    JsonConfigService,
    DynamicListService,
    HttpClient
  ],
  exports: []
})
export class FormlyExtModule {
}
