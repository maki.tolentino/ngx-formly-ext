import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig, FormlyFormOptions} from '@ngx-formly/core';

@Component({
  selector: 'app-dynamic-details-view',
  template: `
    <form [formGroup]="form" (ngSubmit)="submit()">
      <formly-form [model]="model" [fields]="fields" [options]="options" [form]="form">
        <button type="submit" class="btn btn-primary submit-button mr-1">Submit</button>
        <input type="button" class="btn btn-danger" (click)="cancel()" value="Cancel"/>
      </formly-form>
    </form>
  `
})
export class DynamicDetailsViewComponent implements OnChanges, OnInit {
  @Input() itemDetails: any;
  @Input() fields: FormlyFieldConfig[];
  @Output() updateCancelled = new EventEmitter<void>();

  form = new FormGroup({});
  model: any;
  options: FormlyFormOptions = {};
  itemDetailsString: string;

  ngOnChanges() {
    this.model = this.itemDetails;
    this.itemDetailsString = JSON.stringify(this.itemDetails);
  }

  ngOnInit() {
  }

  submit() {
    if (this.form.valid) {
      alert(JSON.stringify(this.model));
    }
  }

  cancel() {
    this.form.clearValidators();
    this.updateCancelled.emit();
  }

}
