import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DynamicDetailsViewComponent} from './dynamic-details-view.component';

describe('DynamicDetailsViewComponent', () => {
  let component: DynamicDetailsViewComponent;
  let fixture: ComponentFixture<DynamicDetailsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DynamicDetailsViewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicDetailsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
