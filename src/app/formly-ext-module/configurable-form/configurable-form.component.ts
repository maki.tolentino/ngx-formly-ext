import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig, FormlyFormOptions} from '@ngx-formly/core';
import {JsonConfigService} from '../common/json-config.service';

@Component({
  selector: 'app-configurable-form',
  template: `
    <div>
      <div class="row">
        <div class="col-12 card mb-3">
          <div class="card-body">
            <h5 class="card-title">
              Configure Form
            </h5>
            <div class="card-text">
              <div class="row">
                <div class="col-md-6">
                  <textarea class="form-control" rows="20" cols="150" #config></textarea>
                </div>
                <div class="col-md-6">
                  <h6>Cache Config</h6>
                  <div class="code flex-nowrap">
                    {{configString}}
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="mt-1">
                  <input type="button" class="btn btn-primary" value="Save Config" (click)="saveConfig(config.value)"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="card mb-3 col-12">
          <div class="card-body">
            <h5 class="card-title">Generated Form</h5>
            <form [formGroup]="form" (ngSubmit)="submit()">
              <formly-form [model]="model" [fields]="fields" [options]="options" [form]="form">
                <button type="submit" class="btn btn-primary submit-button">Submit</button>
              </formly-form>
            </form>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: ['.code { white-space: pre-wrap; font-size: x-small; }']
})
export class ConfigurableFormComponent implements OnInit {
  form = new FormGroup({});
  model: any = {};
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[];
  configString: string;

  constructor(private jsonService: JsonConfigService) {
  }

  submit() {
    if (this.form.valid) {
      alert(JSON.stringify(this.model));
    }
  }

  saveConfig(value: string) {
    const promise = new Promise((resolve, reject) => {
      this.jsonService.setCachedConfig(JSON.parse(value));
      resolve();
    });
    promise.then(() => {
      this.fields = this.jsonService.getCachedConfig();
      this.configString = value;
    });
  }

  ngOnInit(): void {
    this.fields = this.jsonService.getCachedConfig();
    this.configString = this.jsonService.getJsonConfigString('main');
  }

}
