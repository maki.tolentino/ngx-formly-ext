import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ConfigurableFormComponent} from './configurable-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import {JsonConfigService} from '../common/json-config.service';
import {HttpClient, HttpHandler} from '@angular/common/http';

describe('ConfigurableFormComponent', () => {
  let component: ConfigurableFormComponent;
  let fixture: ComponentFixture<ConfigurableFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormlyModule,
        FormlyBootstrapModule
      ],
      declarations: [ConfigurableFormComponent],
      providers: [
        JsonConfigService,
        HttpClient,
        HttpHandler
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurableFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
