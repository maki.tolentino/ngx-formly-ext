import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {JsonConfigService} from '../common/json-config.service';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {DynamicListService, ListData} from "../common/dynamic-list.service";

@Component({
  selector: 'app-dynamic-list',
  templateUrl: './dynamic-list.component.html',
  styles: ['.code { white-space: pre-wrap; }']
})
export class DynamicListComponent implements OnInit {
  @Output() itemSelectedEvent = new EventEmitter<any>();

  tableHeaders: string[];
  itemCodes: string[];
  listItems: any[];
  withIndex: boolean;
  selectedItem: any;
  formConfig: FormlyFieldConfig[];
  data: ListData;
  configListString: any = {};

  constructor(private jsonService: JsonConfigService, private dynamicListService: DynamicListService) {
  }

  setSelectedItem(item: any) {
    this.selectedItem = item;
  }

  ngOnInit() {
    this.data = this.dynamicListService.getListData();
    this.formConfig = this.jsonService.getCachedConfig();
    this.tableHeaders = this.data.headers.items.map(x => x.description);
    this.itemCodes = this.data.headers.items.map(x => x.code);
    this.withIndex = this.data.headers.withIndex;
    this.listItems = this.data.itemValues;
    this.configListString.main = this.jsonService.getJsonConfigString('main');
    this.configListString.alt = this.jsonService.getJsonConfigString('alt');
  }

  onCancelledUpdate() {
    this.selectedItem = null;
  }
}
