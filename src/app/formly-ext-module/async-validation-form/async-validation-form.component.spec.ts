import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AsyncValidationFormComponent} from './async-validation-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import {JsonConfigService} from '../common/json-config.service';
import {HttpClient, HttpHandler} from '../../../../node_modules/@angular/common/http';

describe('AsyncValidationFormComponent', () => {
  let component: AsyncValidationFormComponent;
  let fixture: ComponentFixture<AsyncValidationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormlyModule,
        FormlyBootstrapModule
      ],
      declarations: [AsyncValidationFormComponent],
      providers: [
        JsonConfigService,
        HttpClient,
        HttpHandler
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsyncValidationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
