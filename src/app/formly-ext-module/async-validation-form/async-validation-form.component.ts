import {Component} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {FormlyFieldConfig, FormlyFormOptions} from '@ngx-formly/core';

@Component({
  selector: 'app-async-validation-form',
  template: `
    <div class="row">
      <div class="col-12 card">
        <div class="card-body">
          <h5 class="card-title">
            Existing Emails
          </h5>
          <div class="card-text">
            <ul>
              <li *ngFor="let email of existingEmails">
                {{email}}
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="card mb-3 col-12">
        <div class="card-body">
          <form [formGroup]="form" (ngSubmit)="submit()">
            <formly-form [model]="model" [fields]="fields" [options]="options" [form]="form">
              <button type="submit" class="btn btn-primary submit-button">Submit</button>
            </formly-form>
          </form>
        </div>
      </div>
    </div>`
})
export class AsyncValidationFormComponent {
  form = new FormGroup({});
  model: any = {};
  options: FormlyFormOptions = {};
  existingEmails = [
    'sample01@test.com',
    'sample02@test.com',
    'sample03@test.com'
  ];
  // noinspection TsLint
  fields: FormlyFieldConfig[] = [
    {
      key: 'email',
      type: 'input',
      templateOptions: {
        type: 'email',
        label: 'Email',
        placeholder: 'Email',
        required: true
      },
      validation: {
        messages: {
          required: (error, field: FormlyFieldConfig) => `${field.templateOptions.label} is required`,
          invalidEmailAddress: 'Invalid email address'
        },
      },
      modelOptions: {
        updateOn: 'blur',
      },
      validators: {
        validation: [(control) => {
          if (control.value && control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return null;
          } else {
            return {invalidEmailAddress: true};
          }
        }]
      },
      asyncValidators: {
        uniqueEmailAddress: {
          expression: (control: FormControl) => {
            return new Promise((resolve, reject) => {
              setTimeout(() => {
                if (this.existingEmails.includes(control.value)) {
                  resolve(null);
                } else {
                  resolve({'isEmailExist': true});
                }
              }, 500);
            });
          },
          message: 'Email address already taken'
        }
      }
    }
  ];

  constructor() {
  }

  submit() {
    if (this.form.valid) {
      alert(JSON.stringify(this.model));
    }
  }
}


