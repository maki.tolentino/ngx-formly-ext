import {Injectable} from "@angular/core";

@Injectable()
export class DynamicListService {

  getListData(): ListData {
    return {
      headers: {
        withIndex: true,
        items: [{
          code: 'name',
          description: 'Name'
        }, {
          code: 'email',
          description: 'Email'
        }]
      },
      itemValues: [{
        name: 'John Doe',
        email: 'john.doe@email.com',
        mobileNumber: '+01000001'
      }, {
        name: 'Jane Doe',
        email: 'jane.doe@email.com',
        mobileNumber: '+01000002'
      }, {
        name: 'Juan Doe',
        email: 'juan.doe@email.com',
        mobileNumber: '+01000003'
      }]
    };
  }
}

export interface ListData {
  headers: any;
  itemValues: any;
}
