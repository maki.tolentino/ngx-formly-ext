import {Injectable} from '@angular/core';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class JsonConfigService {
  configList: any = {
    main: [
      {
        'key': 'email',
        'type': 'input',
        'templateOptions': {
          'type': 'email',
          'label': 'Email',
          'placeholder': 'Email',
          'required': true
        }
      },
      {
        'key': 'password',
        'type': 'input',
        'templateOptions': {
          'type': 'password',
          'label': 'Password',
          'placeholder': 'Password',
          'required': true,
          'minLength': 6
        },
        'validation': {
          'messages': {
            'minlength': 'Password should be atleast 6 characters',
          }
        }
      }
    ],
    alt: [
      {
        "key": "email",
        "type": "input",
        "templateOptions": {
          "type": "email",
          "label": "Email",
          "placeholder": "Email",
          "required": true
        }
      },
      {
        "key": "mobileNumber",
        "type": "input",
        "templateOptions": {
          "type": "text",
          "label": "Mobile Number",
          "placeholder": "Mobile Number",
          "required": true,
          "minLength": 11,
          "maxLength": 11
        },
        "validation": {
          "messages": {
            "minlength": "Mobile Number should be 11 digits",
            "maxlength": "Mobile Number should be 11 digits"
          }
        }
      }
    ]
  };
  configListString: any = {};
  cachedConfig: any = this.configList['main'];

  constructor(private http: HttpClient) {
    this.configListString.main = JsonConfigService.prettifyJson(this.configList['main']);
    this.configListString.alt = JsonConfigService.prettifyJson(this.configList['alt']);
  }

  static prettifyJson(jsonString: any) {
    return JSON.stringify(JSON.parse(JSON.stringify(jsonString)), null, '\t');
  }

  getFields(): Observable<FormlyFieldConfig[]> {
    return this.http.get<FormlyFieldConfig[]>('assets/form.config.json');
  }

  getJsonConfig(configKey: string): FormlyFieldConfig[] {
    return (<FormlyFieldConfig[]>(this.configList[configKey]));
  }

  getJsonConfigString(configKey: string): string {
    return this.configListString[configKey];
  }

  setCachedConfig(cachedConfig: any) {
    this.cachedConfig = cachedConfig;
  }

  getCachedConfig() {
    return (<FormlyFieldConfig[]>(this.cachedConfig));
  }
}
