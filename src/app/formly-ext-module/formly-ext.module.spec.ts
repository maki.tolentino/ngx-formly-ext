import {FormlyExtModule} from './formly-ext.module';

describe('FormlyExtModule', () => {
  let formlyExtModuleModule: FormlyExtModule;

  beforeEach(() => {
    formlyExtModuleModule = new FormlyExtModule();
  });

  it('should create an instance', () => {
    expect(formlyExtModuleModule).toBeTruthy();
  });
});
