import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FilePoweredFormComponent} from './file-powered-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import {FormlyModule} from '@ngx-formly/core';
import {JsonConfigService} from '../common/json-config.service';
import {HttpClient, HttpHandler} from '@angular/common/http';

describe('FilePoweredFormComponent', () => {
  let component: FilePoweredFormComponent;
  let fixture: ComponentFixture<FilePoweredFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormlyBootstrapModule,
        FormlyModule
      ],
      declarations: [FilePoweredFormComponent],
      providers: [
        JsonConfigService,
        HttpClient,
        HttpHandler
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilePoweredFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
