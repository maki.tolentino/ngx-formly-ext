import {Component, OnInit} from '@angular/core';
import {FormlyFieldConfig, FormlyFormOptions} from '@ngx-formly/core';
import {FormGroup} from '@angular/forms';
import {JsonConfigService} from '../common/json-config.service';

@Component({
  selector: 'app-file-powered-form',
  template: `
    <div>
      <div class="row mb-3">
        <div class="col-12 card">
          <div class="card-body">
            <h5 class="card-title">
              <p>JSON Config accessed:</p>
              <p>
                <a href="./assets/form.config.json">assets/form.config.json</a>
              </p>
            </h5>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="card mb-3 col-12">
          <div class="card-body">
            <form [formGroup]="form" (ngSubmit)="submit()">
              <formly-form [model]="model" [fields]="fields" [options]="options" [form]="form">
                <button type="submit" class="btn btn-primary submit-button">Submit</button>
              </formly-form>
            </form>
          </div>
        </div>
      </div>
    </div>`
})
export class FilePoweredFormComponent implements OnInit {
  form = new FormGroup({});
  model: any = {};
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[];

  constructor(private jsonConfigService: JsonConfigService) {
  }

  ngOnInit() {
    this.jsonConfigService.getFields().subscribe(fields => this.fields = fields);
  }

  submit() {
    if (this.form.valid) {
      alert(JSON.stringify(this.model));
    }
  }

}
