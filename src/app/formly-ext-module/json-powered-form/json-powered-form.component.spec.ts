import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {JsonPoweredFormComponent} from './json-powered-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import {JsonConfigService} from '../common/json-config.service';
import {HttpClient, HttpHandler} from '@angular/common/http';

describe('ConfigurableFormComponent', () => {
  let component: JsonPoweredFormComponent;
  let fixture: ComponentFixture<JsonPoweredFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormlyModule,
        FormlyBootstrapModule
      ],
      declarations: [JsonPoweredFormComponent],
      providers: [
        JsonConfigService,
        HttpClient,
        HttpHandler
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JsonPoweredFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
