import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig, FormlyFormOptions} from '@ngx-formly/core';
import {JsonConfigService} from '../common/json-config.service';

@Component({
  selector: 'app-json-powered-form',
  template: `
    <div>
      <div class="row mb-3">
        <div class="col-12 card">
          <div class="card-body">
            <h5 class="card-title">
              Sample JSON Config
            </h5>
            <div class="card-text code">
              {{jsonConfig}}
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="card mb-3 col-12">
          <div class="card-body">
            <h5 class="card-title">Generated Form</h5>
            <form [formGroup]="form" (ngSubmit)="submit()">
              <formly-form [model]="model" [fields]="fields" [options]="options" [form]="form">
                <button type="submit" class="btn btn-primary submit-button">Submit</button>
              </formly-form>
            </form>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: ['.code { white-space: pre-wrap; }']
})
export class JsonPoweredFormComponent implements OnInit {
  form = new FormGroup({});
  model: any = {};
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[];
  jsonConfig: any;

  constructor(private jsonService: JsonConfigService) {
  }

  submit() {
    if (this.form.valid) {
      alert(JSON.stringify(this.model));
    }
  }

  ngOnInit(): void {
    this.fields = this.jsonService.getJsonConfig('main');
    this.jsonConfig = this.jsonService.getJsonConfigString('main');
  }

}
