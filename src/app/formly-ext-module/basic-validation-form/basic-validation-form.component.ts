import {Component} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig, FormlyFormOptions} from '@ngx-formly/core';


@Component({
  selector: 'app-basic-validation-form',
  template: `
    <div class="row">
      <div class="card mb-3 col-12">
        <div class="card-body">
          <form [formGroup]="form" (ngSubmit)="submit()">
            <formly-form [model]="model" [fields]="fields" [options]="options" [form]="form">
              <button type="submit" class="btn btn-primary submit-button">Submit</button>
            </formly-form>
          </form>
        </div>
      </div>
    </div>
  `
})
export class BasicValidationFormComponent {
  form = new FormGroup({});
  model: any = {};
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [
    {
      key: 'email',
      type: 'input',
      templateOptions: {
        type: 'email',
        label: 'Email',
        placeholder: 'Email',
        required: true
      },
      validation: {
        messages: {
          required: (error, field: FormlyFieldConfig) => `${field.templateOptions.label} is required`,
          invalidEmailAddress: 'Invalid email address',
        },
      },
      validators: {
        validation: [(control) => {
          if (control.value && control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return null;
          } else {
            return {invalidEmailAddress: true};
          }
        }]
      }
    },
    {
      key: 'username',
      type: 'input',
      templateOptions: {
        label: 'Username',
        placeholder: 'Username',
        required: true,
        maxLength: 30
      },
      validation: {
        messages: {
          maxlength: (error, field: FormlyFieldConfig) => `${field.templateOptions.label} should be less than 30 characters`
        }
      }
    },
    {
      key: 'password',
      type: 'input',
      templateOptions: {
        type: 'password',
        label: 'Password',
        placeholder: 'Password',
        required: true,
        minLength: 6
      },
      validation: {
        messages: {
          minlength: (error, field: FormlyFieldConfig) => `${field.templateOptions.label} should be more than ${field.templateOptions.minLength} characters`,
        }
      }
    },
    {
      key: 'textarea',
      type: 'textarea',
      templateOptions: {
        label: 'Comments',
        placeholder: 'Type a comment here',
        rows: 4,
        required: true
      }
    },
    {
      key: 'isNewAccount',
      type: 'checkbox',
      templateOptions: {
        label: 'New Account?',
        placeholder: 'Age',
        required: true
      }
    },
    {
      key: 'civilStatus',
      type: 'select',
      templateOptions: {
        label: 'Civil Status',
        options: [
          {label: 'Never married', value: 'unmarried'},
          {label: 'Widowed', value: 'widowed'},
          {label: 'Divorced', value: 'divorced'},
          {label: 'Separated', value: 'separated'},
          {label: 'Married', value: 'married'}
        ],
        required: true
      }
    },
    {
      key: 'personType',
      type: 'radio',
      templateOptions: {
        label: 'Dog or Cat Person?',
        options: [
          {label: 'Dog', value: 'dog'},
          {label: 'Cat', value: 'cat'}
        ],
        required: true
      }
    }
  ];

  constructor() {
  }

  submit() {
    if (this.form.valid) {
      alert(JSON.stringify(this.model));
    }
  }

}
