import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BasicValidationFormComponent} from './basic-validation-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import {JsonConfigService} from '../common/json-config.service';
import {HttpClient, HttpHandler} from '../../../../node_modules/@angular/common/http';

describe('BasicValidationFormComponent', () => {
  let component: BasicValidationFormComponent;
  let fixture: ComponentFixture<BasicValidationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormlyModule,
        FormlyBootstrapModule
      ],
      declarations: [BasicValidationFormComponent],
      providers: [
        JsonConfigService,
        HttpClient,
        HttpHandler
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicValidationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
