import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BasicFormComponent} from './basic-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import {FormlyModule} from '@ngx-formly/core';

describe('BasicFormComponent', () => {
  let component: BasicFormComponent;
  let fixture: ComponentFixture<BasicFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BasicFormComponent],
      imports: [
        ReactiveFormsModule,
        FormlyBootstrapModule,
        FormlyModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Should create component', () => {
    expect(component).toBeTruthy();
  });

  it('Should display text input', () => {
    const element: HTMLElement = fixture.nativeElement;
    const input: HTMLInputElement = element.querySelector('input');
    expect(input.type).toBe('email');
  });
});
