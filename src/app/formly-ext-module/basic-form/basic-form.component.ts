import {Component} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig, FormlyFormOptions} from '@ngx-formly/core';

@Component({
  selector: 'app-basic-form',
  template: `
    <div class="row">
      <div class="card mb-3 col-12">
        <div class="card-body">
          <form [formGroup]="form" (ngSubmit)="submit()">
            <formly-form [model]="model" [fields]="fields" [options]="options" [form]="form">
              <button type="submit" class="btn btn-primary submit-button">Submit</button>
            </formly-form>
          </form>
        </div>
      </div>
    </div>
  `
})
export class BasicFormComponent {
  form = new FormGroup({});
  model: any = {};
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [
    {
      key: 'email',
      type: 'input',
      templateOptions: {
        type: 'email',
        label: 'Email',
        placeholder: 'Email'
      }
    },
    {
      key: 'username',
      type: 'input',
      templateOptions: {
        label: 'Username',
        placeholder: 'Username'
      }
    },
    {
      key: 'password',
      type: 'input',
      templateOptions: {
        type: 'password',
        label: 'Password',
        placeholder: 'Password'
      }
    },
    {
      key: 'birthDate',
      type: 'input',
      templateOptions: {
        type: 'date',
        label: 'Date of Birth'
      }
    },
    {
      key: 'textarea',
      type: 'textarea',
      templateOptions: {
        label: 'Comments',
        placeholder: 'Type a comment here',
        rows: 4
      }
    },
    {
      key: 'age',
      type: 'input',
      templateOptions: {
        type: 'number',
        label: 'Age',
        placeholder: 'Age'
      }
    },
    {
      key: 'isNewAccount',
      type: 'checkbox',
      templateOptions: {
        label: 'New Account?',
        placeholder: 'Age'
      }
    },
    {
      key: 'civilStatus',
      type: 'select',
      templateOptions: {
        label: 'Civil Status',
        options: [
          {label: 'Never married', value: 'unmarried'},
          {label: 'Widowed', value: 'widowed'},
          {label: 'Divorced', value: 'divorced'},
          {label: 'Separated', value: 'separated'},
          {label: 'Married', value: 'married'}
        ]
      }
    },
    {
      key: 'civilStatus',
      type: 'select',
      templateOptions: {
        label: 'Civil Status',
        options: [
          {label: 'Never married', value: 'unmarried'},
          {label: 'Widowed', value: 'widowed'},
          {label: 'Divorced', value: 'divorced'},
          {label: 'Separated', value: 'separated'},
          {label: 'Married', value: 'married'}
        ]
      }
    },
    {
      key: 'personType',
      type: 'radio',
      templateOptions: {
        label: 'Dog or Cat Person?',
        options: [
          {label: 'Dog', value: 'dog'},
          {label: 'Cat', value: 'cat'}
        ]
      }
    }
  ]
  ;

  constructor() {
  }

  submit() {
    if (this.form.valid) {
      alert(JSON.stringify(this.model));
    }
  }
}
