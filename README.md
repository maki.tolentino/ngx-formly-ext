# NgxFormlyExt

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

### Dev Notes
#### TODO
* ~~Update ng cli and core version~~
* ~~Create basic component that uses ngx formly module~~
* ~~Add basic component test~~
* ~~Add route to "Basic Form" example~~
* ~~Add route to "Basic Validation" example~~
* ~~Add route to "Async Validation" example~~
* ~~Add route to "JSON Dynamic Form" example~~
* ~~Add route to "File Input Form" example~~
* ~~Add more input field types to basic form page~~
  * password field
  * dropdown
  * radio button
  * text area
  * date picker
  * email
  * checkbox
  * number
* ~~Update basic validation component~~
* ~~Update async validation component~~
* ~~Update JSon powered component~~
* ~~Update File input powered component~~
* ~~Create http async get observable for json service~~
* ~~Export as an NgModule~~
* ~~Create sample of configurable form~~
* ~~Create list view component~~
* Create detailed view component


#### Low Prio
* Add e2e tests
* Add theming page
* Add dynamic list page with form

#### Basic Functionality Specs 
Phase 1: Basic Functionality
Technical Requirements:
1. It should be an Angular NPM Module
2. It should be implemented in TypeScript
3. It should be themeable. Look and feel could be changed via external CSS or other CSS-related solutions

Functional Requirements
1. The component should render the form based on metadata. The metadata consists of the following:
  - data type
  - label
  - layout
  - list of values ( source should be configurable as well)
2. Metadata source should be configurable, it could be a .json file as payload, from a rest endpoint, etc.
3. Should support in-line function-based validator
